<?php

class User
{
    private $name;
    private $balance;
    
    public function __construct($name, $balance){
        $this->name = $name;
        $this->balance = $balance;
    }
    
    public function getName(){
        return $this->name;
    }
    public function getBalance(){
        return $this->balance;
    }

    public function printStatus(){
        return 'У пользователя '. $this->getName().' сейчас на счету '. $this->getBalance().'<br>';
    }
    
    public function giveMoney($getMoneyUser, $amount){
        if($this->balance >= $amount){
            $this->balance -= $amount;
        }
        
        $getMoneyUser->balance += $amount;
        
        return '<hr>Пользователь '. $this->getName().' перечислил '.$amount.' пользователю '.$getMoneyUser->getName().'<br><hr>';
        
    }
    
}

$user1 = new User('Vasya', 5000);
echo $user1->printStatus();

$user2 = new User('Petya', 3000); 
echo $user2->printStatus();

echo $user1->giveMoney($user2, 1000);

echo $user1->printStatus();
echo $user2->printStatus();

echo $user2->giveMoney($user1, 1500);
echo $user1->printStatus();
echo $user2->printStatus();